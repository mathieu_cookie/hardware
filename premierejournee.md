Récap de la séance 26/10
```
Début de séance : 

    - présentation personnel intégrale
    - explication du ydays 
    - pause petit déjeuner 
 
Lancement du tp : 

    - Avec des tours de pc fixe imposés, nous devions démonter les composants de l'ordinateur puis une fois tout les composants enlevés, les remettres en place avec les bons composants au bon endroit et bien branché, fut notre deuxième tâche de la journée.

Fin du tp : 

    - un petit compte rendu a été rédigé 

Points positifs : 

    - manipulation d'un ordinateur et de tout ses composants
    - autonomie mais malgré tout de l'aide si nous en avions besoin 
    - une bonne compréhension lors de l'explication de la diapo donc une bonne maniabilité lors du tp 
    - une bonne ambiance de travail

Point négatif : 

    - aucun :)

```